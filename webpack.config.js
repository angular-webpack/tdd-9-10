const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');

const config = {
  mode: 'development',
  entry: {
    main: './app/main.ts',
    styles: './styles/main.css'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.[name].js'
  },
  module: {
    rules: [
      {// turn off webpack warning "System.import() is deprecated..."
        test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
        parser: { system: true }
      },
      {
        test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
        loader: '@ngtools/webpack'
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/[name].[contenthash].[ext]'
      },
      {
        test: /\.css$/,
        exclude: /app/,
        // use: [MiniCssExtractPlugin.loader, 'css-loader']
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.component\.css$/, // used for css files that are linked through styleUrls
        loader: 'raw-loader'
      },
      {
        test: /\.js$/, // clean browser warnings related to sourcemaps
        use: ["source-map-loader"],
        enforce: "pre"
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  plugins: [
    new AngularCompilerPlugin({
        tsConfigPath: './tsconfig.json',
        skipCodeGeneration: true,
    }),
    new HtmlWebpackPlugin({
      template: './index.html'
    })
  ],
  devServer: {
    port: 9000
  }
};

module.exports = config;