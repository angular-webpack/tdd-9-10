const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = {
  mode: 'production',
  entry: {
    main: './app/main.ts',
    styles: './styles/main.css'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.[name].js'
  },
  module: {
    rules: [
      {
        test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
        loader: '@ngtools/webpack'
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          // Fixes:
          // - "Can't bind to 'ngforOf' since it isn't a known property of 'div'"
          // - img tag gets parsed as a normal string
          minimize: false
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/[name].[hash].[ext]'
      },
      {
        test: /\.css$/,
        exclude: /app/,
        // use: [MiniCssExtractPlugin.loader, 'css-loader']
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.component\.css$/, // used for css files that are linked through styleUrls
        loader: 'raw-loader'
      },
      {
        test: /\.js$/, // clean browser warnings related to sourcemaps
        use: ["source-map-loader"],
        enforce: "pre"
      }
    ]
  },
  resolve: {
    extensions: [
      '.ts',
      '.js'
    ]
  },
  plugins: [
    new AngularCompilerPlugin({
      tsConfigPath: './tsconfig.json',
      
      // @source https://medium.com/angular-in-depth/do-you-know-how-angular-transforms-your-code-7943b9d32829
      mainPath: './app/main.ts', // allow replace_bootstrap.ts to create aot version of main ts
      compilerOptions: {
        enableIvy: false
      }
    }),
    new HtmlWebpackPlugin({
      template: './index.html'
    }),
    new BundleAnalyzerPlugin()
  ],
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0, // very important
      cacheGroups: {
        vendor: { // @source: https://medium.com/hackernoon/the-100-correct-way-to-split-your-chunks-with-webpack-f8a9df5b7758
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
  
              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace('@', '')}`;
            },
          }, 
      }
    },
  },
};

module.exports = config;
