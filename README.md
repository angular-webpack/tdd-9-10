## Law of TDD

* Red: we want to make changes to the component, but it is taking too long
* Green: we will use webpack dev server and configure it to hot reload our changes
* Refactor: Now we can make changes to the component.

To serve the app during development

```sh
npm run serve
```

You can also serve a production build

```sh
npm run serve:prod
```

Setup commands

```sh
npm i -D -E webpack-dev-server@v3.10.3
```

TODO: hot reload