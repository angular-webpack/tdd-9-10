const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');
const { BuildOptimizerWebpackPlugin } = require('@angular-devkit/build-optimizer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const { SuppressExtractedTextChunksWebpackPlugin } = require('./webpack-helpers/suppress-entry-chunks-webpack-plugin');

const config = {
  mode: 'production',
  target: 'web',
  entry: {
    main: './app/main.ts',

    // https://github.com/webpack-contrib/mini-css-extract-plugin/issues/151
    styles: './styles/main.css',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[contenthash].js'
  },
  module: {
    strictExportPresence: true,
    rules: [
      {// turn off webpack warning "System.import() is deprecated..."
        test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
        parser: { system: true }
      },
      {
        test: /\.js$/,
        exclude: /(ngfactory|ngstyle)\.js$/,
        loader: '@angular-devkit/build-optimizer/webpack-loader',
        options: {
          sourceMap: false
        }
      },
      {
        test: /(?:\.ts)$/, // for ivy
        // test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/, // for view engine
        use: [
          {
            loader: '@angular-devkit/build-optimizer/webpack-loader',
            options: {
              sourceMap: false
            }
          },
          {
            loader: '@ngtools/webpack',
          }
        ]
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          // Fixes:
          // - "Can't bind to 'ngforOf' since it isn't a known property of 'div'"
          // - img tag gets parsed as a normal string
          minimize: false
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/[name].[contenthash].[ext]'
      },
      {
        test: /\.css$/,
        include: /styles/,
        use: [MiniCssExtractPlugin.loader, { loader: 'css-loader', options: { url: true, sourceMap: false } }]
      },
      {
        test: /\.component\.css$/, // used for css files that are linked through styleUrls
        exclude: /styles/,
        loader: 'raw-loader'
      },
      {
        test: /\.js$/, // clean up browser warnings related to sourcemaps
        use: ["source-map-loader"],
        enforce: "pre"
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.tsx', '.mjs', '.js'],
    mainFields: [ 'es2015', 'browser', 'module', 'main' ] // useful if you are targeting browser with es6 support
  },
  plugins: [
    new CleanWebpackPlugin(),
    new AngularCompilerPlugin({
      tsConfigPath: './tsconfig.json',
      skipCodeGeneration: false,

      // @source https://medium.com/angular-in-depth/do-you-know-how-angular-transforms-your-code-7943b9d32829
      mainPath: './app/main.ts', // allow replace_bootstrap.ts to create aot version of main ts
      
      // directTemplateLoading: true, // prevents webpack from creating hashed name for images/asset files
      compilerOptions: {
        enableIvy: true,
        "fullTemplateTypeCheck": true,
        "strictInjectionParameters": true,
      }
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
    }),
    new BuildOptimizerWebpackPlugin(),
    new MiniCssExtractPlugin({
        filename: 'styles.[contenthash].css',
        ignoreOrder: false,
    }),
    new SuppressExtractedTextChunksWebpackPlugin()
  ],
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all', // very important
      maxInitialRequests: Infinity,
      minSize: 0, // very important
      cacheGroups: {
        vendor: { // @source: https://medium.com/hackernoon/the-100-correct-way-to-split-your-chunks-with-webpack-f8a9df5b7758
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
  
              if(packageName.includes('angular')) {
                console.log(module.context);
              }
              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace('@', '')}`;
            },
          }, 
      }
    },
    usedExports: true, // remove references to unused exported functions/classes (ex: the minus function)
    sideEffects: true,
    minimize: true, // <= use specified minimizer to cleanup the output
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        sourceMap: false,
        terserOptions: {
        output: {
          beautify: false
        },
        compress: {
          unused: true, // <= get rid of unused functions/variables if available
          passes: 3,
          // Taken from ng-cli config: removes around 40KB
          // @source: TODO build_angular/src/utils/webpack-browser-config.js
          global_defs: { ngDevMode: false, ngI18nClosureMode: false, ngJitMode: false }
        },
        mangle: true
      }
      })
    ],
  },
};

module.exports = config;
