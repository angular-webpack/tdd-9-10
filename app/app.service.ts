import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

// import { tale } from './test-data';

export type Page = {
    pageNumber: number, // a page takes over two sides of a paper
    sideNumber?: number, // side refer to a side of a paper
    isPageIndicator?: boolean,
    headers: Array<string>,
    paragraphs: Array<string>,
}

@Injectable({
    providedIn: 'root'
})
export class AppService {
    constructor(private readonly http: HttpClient){}

    getTitlePage(): Observable<Page> {
        const page: Page = {
            pageNumber: 0,
            headers: [
                'Kibogo',
                'na',
                'Rutegaminsi'
            ],
            paragraphs: [],
        }

        return of(page);
    }

    getEndPage(): Observable<Page> {
        const page: Page = {
            pageNumber: 0,
            headers: [
                'Sinjye',
                'Wahera'
            ],
            paragraphs: [],
        }

        return of(page);
    }

    private generateParagraphs(): Observable<Array<string>> {

        const TALE_ID = 135;
        const TALE_HOST = 'https://myway.kgltimes.com';
        const TALE_RESOURCE_NAMESPACE = 'wp-json/wp/v2/stories';
        const TALE_ENDPOINT = `${TALE_HOST}/${TALE_RESOURCE_NAMESPACE}/${TALE_ID}?purpose=paragraphs`;

        return this.http.get<{title:string, content: Array<string> }>(TALE_ENDPOINT).pipe(
            map( ({ content }) => content)
        );
    }


    /**
     * Tell me where you are going to put these pages and I will give them to you
     * @param pageHeight 
     * @param spacing space between one paragraph and the next 
     * @param playground 
     */
    getPages(pageHeight: number, spacing: number, playground: HTMLElement): Observable<Array<Page>> {

        const title$ = this.getTitlePage();
        const end$ = this.getEndPage();

        const paragraphs$ = this.generateParagraphs();
        const mainPages$ = paragraphs$.pipe(
            map((paragraphs) => {
                let pages: Array<Page> = [];
                let pageCount = 1;
                do {
                    const page = this.createPage(pageHeight, spacing, paragraphs, playground);
                    
                    page.pageNumber = ++pageCount;
                    pages = [...pages, page]

                    if(paragraphs.length && !page.paragraphs.length) throw paragraphs[0];

                    paragraphs.splice(0, page.paragraphs.length);
                } while(paragraphs.length)

                return pages;
            })
        );


        const tale$ = combineLatest(title$, end$, mainPages$);

        return tale$.pipe(
            map(([titlePage, endPage, mainPages]) => {
                return this.prepareForDisplay([
                    { ...titlePage, pageNumber: 1 },
                    ...mainPages,
                    { ...endPage, pageNumber: mainPages.length + 1 }
                ]);
            })
        )
    }

    /**
     * Take a list of paragraphs and the dimension of a page interface
     * 
     * Picks only paragraphs that can fit on the page.
     * 
     * NOTES
     * - need to know how many lines a paragraph can take based on the width of the page and the number of words in the paragraph
     * 
     * @return 
     */
    private createPage(pageHeight: number, spacing: number, paragraphs: Array<string>, playground: HTMLElement): Page {
        const page: Page = {
            pageNumber: 0, // a page takes over two sides of a paper
            headers: [],
            paragraphs: [],
        }

        let totalParagraphHeight = 0;
        for (let i = 0; i < paragraphs.length; i++) {
            const paragraph = paragraphs[i];

            const paragraphHeight = this.computeParagraphHeight(paragraph, playground);
            totalParagraphHeight += paragraphHeight + spacing;

            const runOutofSpace = totalParagraphHeight > pageHeight;
            if (runOutofSpace) break;

            // add paragraph to page.paragraphs list
            page.paragraphs.push(paragraph);
        }

        return page;
    }

    private computeParagraphHeight(paragraph: string, playground: HTMLElement) {
        // get the height of one line
        playground.textContent = paragraph;
        const base = playground.getBoundingClientRect().height;
        return base;
    }

    prepareForDisplay(pages: Array<Page>): Array<Page> {
        return pages.reduce((acc, page, idx, arr) => {
            const pageIndicator = {
                pageNumber: page.pageNumber, // a page takes over two sides of a paper
                sideNumber: acc.length + 1, // side refer to a side of a paper
                isPageIndicator: true,
                headers: [],
                paragraphs: [],
            };
            return arr.length - 1 === idx ?
                [...acc, { ...page, sideNumber: acc.length }] :
                [...acc, { ...page, sideNumber: acc.length }, pageIndicator];
        }, [] as Array<{
            pageNumber: number, // a page takes over two sides of a paper
            sideNumber?: number, // side refer to a side of a paper
            isPageIndicator?: boolean,
            headers: Array<string>,
            paragraphs: Array<string>,
        }>).reverse()
    }
}