export const tale = 
`
    Umugabo Kibogo atuye ku ijuru, agategeka ibihari byose n' imvura. 
    Umwami w' u Rwanda Mutara wa Yuhi, akagira umugaragu witwa Rutegaminsi. 
    Bukeye imvura irabura, amapfa aratera, ibintu byose biruma. 
    Umunsi umwe Mutara agenda mu Rwanda rwe ahura na Kibogo, aramubaza ati: "Kibogo igituma imvura itagwa ni iki?".
    "Aho si wowe utuma itagwa, wowe utuye ku ijuru?".
    Mutara ageze iwe, ahamagara Rutegaminsi, amutuma kwa Kibogo ngo ajye kumubaza impamvu ituma imvura itagwa. 
    Rutegaminsi aragenda, maze igitagangurirwa kiboha urudodo rukomeye, Rutegaminsi arugenderaho agera ku ijuru. Ahageze ijuru ryanga gukinguka;
    inkuba irakubita, ijuru rirakinguka Rutegaminsi arinjira. 
    Ageze kwa Kibogo ati: "Ndashaka ubuhake".
    Bati: "Ngwino utwasirize inkwi". 
    Rutegaminsi ati: "Ariko nazasa nagira, mbasabye umugeni hakiri kare".
    Baramwemerera.
    Rutegaminsi yemera umurimo wo kwasa inkwi. 
    Buracya bamuha intorezo ngo najye kwasa urutare baza kumwereka.
    Rutegaminsi aragenda no ku rutare bamweretse, urutare arukubita intorezo, icyuma gisubira ku mutwe. 
    Nuko inkuba iraza irarukubita ishyira imyase aho.
    Rutegaminsi abura imigozi yo guhambira imyase y' urutare.
    Agiye kubona, abona inzoka n' abana bayo biramubwira ngo birarambarara hasi maze abihambirize, agende nagera ku kibero cy' inzu kwa Kibogo, ashyire hasi zirukire mu nzu. 
    Rutegaminsi arahambira arangije arikorera, aragenda; babona azanye imyase y' urutare.
    Bukeye baramubwira ngo najye guhinga ishyamba kandi arimare; 
    Rutegaminsi aragenda na none atazi uko ari bubigenze. 
    Ahageze, ifuku ziraza ziti: "Reka tukwereke", Zirahayogoza.
    Baje basanga intabire hose, barumirwa!.
    Rutegaminsi ati: "Nimumpe umugeni".
    Bati: "Tuzamuguha ejo".
    Bajya inama yo gutwikira mu nzu Rutegaminsi ngo apfe.
    Inzoka zirabyumva, ziragenda zibwira Rutegaminsi ngo yigire mu gikari, naho ubundi baramutwikira mu nzu.
    Rutegaminsi abonye bumaze kwira yigira mu gikari.
    Inzu barayishumika bibwira ko aza guhiramo.
    Mu gitondo basanga yashashe mu muryango ariho aryamye, baratangara.
    Barongera bajya indi nama, bati: "Noneho dukoranye abakobwa benshi turebe ko azamenya umukobwa twamugeneye uwo ari we, namuyoberwa tumumwime".
    Isazi ibyumva nk' ejo, ibarira Rutegaminsi, iti: "Maze uze kureba uwo ngwaho akanyiyama, ni we uza kuba ari uwawe, uze kumufata, umubwire uti: "Ndakurongoye".
    Bazana abakobwa, isazi igwa k'uwa Rutegaminsi, aramufata aramujyana aramurongora.
    Rutegaminsi amaranye n' umugore we iminsi itandatu aramubaza ati: "Ntiwandusha kumenya igituma imvura itakigwa?".
    Umugore aramusubiza ati "Ni Kibogo wayibitse, mu nzu ye hamanitse ingoma, maze iyo ngoma uwayikubitaho umurishyo imvura yagwa". 
    Rutegaminsi ahamagara umugaragu wa Kibogo aramubwira ati: "Genda unzanire ingoma ya Kibogo ndaguhemba". 
    Umugaragu aragenda, asanga kwa Kibogo bose basinziriye, aratambuka azana ingoma buhoro, ayiha Rutegaminsi.
    Nuko Rutegaminsi ingoma ayikomaho umurishyo, ako kanya imvura iragwa.
    Rutegaminsi aragenda ajya kubwira umwami Mutara wa Yuhi ko avuye kumanura imvura ku ijuru. 
    Umwami aramushima, amugororera inka nyinshi amuha n' imisozi arayitwara
`;