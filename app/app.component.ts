import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core'
import { AppService, Page } from './app.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const PAGES_EN = [
    {
        pageNumber: 1,
        headers: [
            `Kibogo`,
            `and`,
            `Rutegaminsi`,
        ],
        paragraphs: []
    },
    {
        pageNumber: 2,
        paragraphs: [
            `The man Kibogo had inhabited heaven, he ruled everything there even rain.`,
            `The king of Rwanda Mutara of yuhi had a servant called Rutegaminsi.`,
            `The next morning rain disappeared the country hit by a severe drought, everything dried.`,
            `One day Mutara met Kibogo and ask him: "why has rain disappeared?"`,
            `and said again: "isn't it you who make it not rain?"`,
        ]
    },
    {
        pageNumber: 3,
        paragraphs: [
            `When Mutara reached home, he called Rutegaminsi and sent him to Kibogo to ask him why it's not raining.`,
            `A spider made a strong string and Rutegaminsi walked along on it up to heaven.`,
            `When he got there heaven denied to be open, thunder hit it and heaven open Rutegaminsi entered .`,
            `At Kibogo's home he said" I want to be a servant"`,
            `They said "come and cut wood for us"`,
        ]
    },
    {
        pageNumber: 4,
        paragraphs: [
            `Rutegaminsi said "i'm going to do it, but I request a bride as my compensation."`,
            `They accepted it.`,
            `The next morning, Rutegaminsi was given an axe.`,
            `They told him to go and split a rock into pieces.`,
        ]
    },
    {
        pageNumber: 5,
        headers: [
            `The`,
            `End`,
        ],
        paragraphs: []
    },
];

const PAGES_RW = [
    {
        pageNumber: 1,
        headers: [
            `Kibogo`,
            `na`,
            `Rutegaminsi`,
        ],
        paragraphs: []
    },
    {
        pageNumber: 2,
        paragraphs: [
            `Umugabo Kibogo atuye ku ijuru, agategeka ibihari byose n' imvura.`,
            `Umwami w' u Rwanda Mutara wa Yuhi, akagira umugaragu witwa Rutegaminsi.`,
            `Bukeye imvura irabura, amapfa aratera, ibintu byose biruma.`,
            `Umunsi umwe Mutara agenda mu Rwanda rwe ahura na Kibogo, Aramubaza ati: "Kibogo igituma imvura itagwa ni iki?"`,
            `"Aho si wowe utuma itagwa, wowe utuye ku ijuru?"`
        ]
    },
    {
        pageNumber: 3,
        paragraphs: [
            `Mutara ageze iwe, ahamagara Rutegaminsi, amutuma kwa Kibogo ngo ajye kumubaza impamvu ituma imvura itagwa.`,
            `Rutegaminsi aragenda, maze igitagangurirwa kiboha urudodo rukomeye, Rutegaminsi arugenderaho agera ku ijuru.`,
            `Ahageze ijuru ryanga gukinguka; inkuba irakubita, ijuru rirakinguka Rutegaminsi arinjira.`,
            `Ageze kwa Kibogo ati: "Ndashaka ubuhake."`,
            `Bati: "Ngwino utwasirize inkwi."`,
        ]
    },
    {
        pageNumber: 4,
        paragraphs: [
            `Rutegaminsi ati: "Ariko nazasa nagira, mbasabye umugeni hakiri kare".`,
            `Baramwemerera.`,
            `Rutegaminsi yemera umurimo wo kwasa inkwi.`,
            `Buracya bamuha intorezo ngo najye kwasa urutare baza kumwereka.`,
        ]
    },
    {
        pageNumber: 5,
        headers: [
            `Sinjye`,
            `Wahera`,
        ],
        paragraphs: []
    },
];

const PADDING = 20;
const SPACING = 16; // = bottom margin

@Component({
    selector: 'tuvuge-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

    // @ts-ignore
    @ViewChild('book') book: ElementRef<HTMLDivElement>;
    // @ts-ignore
    @ViewChild('PDPlaceholder') PD_Placeholder: ElementRef<HTMLParagraphElement>;

    constructor(private readonly appService: AppService){}

    ngOnInit(): void {
        console.log(this.pages$);
        // const poInt = setInterval(() => {
        //     this.next(19);
        //     if (this.nextPage > 19) clearInterval(poInt);
        // }, 5000);
    }

    ngAfterViewInit(): void {
        setTimeout(() => { // @TODO replace with font loaded event
            const pageHeight = this.book.nativeElement.getBoundingClientRect().height - (2*PADDING);
            this.pages$ = this.appService.getPages(pageHeight, SPACING, this.PD_Placeholder.nativeElement);
        }, 3000);
    }

    private openPages: Array<number> = [];
    private nextPage = 0; // we are assuming that the first sideNumber is 0

    isOpen(page: number): boolean {
        return this.openPages.includes(page);
    }
    next(pageCount:number) {
        const hasNext = this.nextPage < pageCount;
        // const hasNext = this.nextPage < this.pages.length - 1;
        if (!hasNext) {
            throw "oops";
        }
        this.openPages.push(this.nextPage);
        this.openPages.push(this.nextPage + 1);
        this.nextPage = this.nextPage + 2;
    }
    prev() {
        this.openPages.pop();
        this.openPages.pop();
        this.nextPage = this.nextPage - 2;
    }

    pages$: Observable<Page[]> = of([]);

    // // content
    // pages: Array<{
    //     pageNumber: number, // a page takes over two sides of a paper
    //     sideNumber?: number, // side refer to a side of a paper
    //     isPageIndicator?: boolean,
    //     headers?: Array<string>,
    //     paragraphs?: Array<string>,
    // }> = PAGES_RW.reduce((acc, page, idx, arr) => {
    //     const pageIndicator = {
    //         pageNumber: page.pageNumber, // a page takes over two sides of a paper
    //         sideNumber: acc.length + 1, // side refer to a side of a paper
    //         isPageIndicator: true,
    //     };
    //     return arr.length - 1 === idx ?
    //         [...acc, { ...page, sideNumber: acc.length }] :
    //         [...acc, { ...page, sideNumber: acc.length }, pageIndicator];
    // }, [] as Array<{
    //     pageNumber: number, // a page takes over two sides of a paper
    //     sideNumber?: number, // side refer to a side of a paper
    //     isPageIndicator?: boolean,
    //     headers?: Array<string>,
    //     paragraphs?: Array<string>,
    // }>).reverse()
}
